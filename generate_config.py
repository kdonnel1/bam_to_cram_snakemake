#!/bin/python

#############################################################################
#Generate config.yaml from input bam paths for cram conversion
#############################################################################

import sys
import os
import argparse

def generate_config(bamPaths,ref):

	bamFiles = {}

	with open(bamPaths,'r') as inBams:
		for path in inBams:
			bam = path.split('/')[-1]
			sampleName = os.path.splitext(bam)[0]
			bamFiles[sampleName] = path

	with open ('config.yaml','w') as outYaml:
		
		#Define reference
		msg = 'reference_genome: ' + ref + '\n'
		outYaml.write(msg)
		outYaml.write('\n')

		doubleSpace = '  '

		#Define bamfiles
		outYaml.write('bamfiles:\n')
		for key, value in bamFiles.items():
			msg = doubleSpace + key + ': ' + value
			outYaml.write(msg)
		outYaml.write('\n')

		#Define target files
		outYaml.write('target_files:\n')
		for key, value in bamFiles.items():
			target_dir = os.path.dirname(value)
			target_file = key + '.conv.log'
			msg = doubleSpace + key + ': ' + target_dir + '/' + target_file + '\n'
			outYaml.write(msg)
		 
def main():

	parser = argparse.ArgumentParser(description='Generate config.yaml from input bam paths')

	parser.add_argument('bam_paths',action='store',help='Text file defining input bam paths, one per line')
	parser.add_argument('-ref_fasta',action='store',help='Path to reference fasta (default hg38)',default='/gpfs/igmmfs01/software/pkg/el7/apps/bcbio/share2/genomes/Hsapiens/hg38/seq/hg38.fa')

	args = parser.parse_args()
	generate_config(args.bam_paths,args.ref_fasta)

#Global Variables
#Kick it off
main()
