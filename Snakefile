configfile: "config.yaml"
conda: "environment.yaml"


#Obtain target outputs from config file
def get_target_files():
	targets = list()
	for target in config["target_files"]:
		targets.append(config["target_files"][target])
	return targets

#Requires all validation to be complete before transfering conversion log file
rule all:
	input: get_target_files()

#Retrieve bamfiles from datastore (assumes index follows convention sample.bam.bai)
rule retrieve_input:
	input:
		bam = lambda wildcards: config["bamfiles"][wildcards.sample]
	output:
		bam = {"work/{sample}.bam"},
		bam_index = {"work/{sample}.bam.bai"},
		bam_md5 = {"work/{sample}.bam.md5"},
		bam_index_md5 = {"work/{sample}.bam.bai.md5"}
	log:
		"logs/{sample}.rsync.from.datastore.log"
	resources:
		runtime = 300
	run:
		shell("rsync -v {input.bam} {output.bam} 2> {log}")
		shell("rsync -v {input.bam}.bai {output.bam_index} 2> {log}")
		shell("rsync -v {input.bam}.md5 {output.bam_md5} 2> {log}")
		shell("rsync -v {input.bam}.bai.md5 {output.bam_index_md5} 2> {log}")

#Check incoming bam md5 files
rule check_bam_md5:
	input:
		bam_md5 = "work/{sample}.bam.md5",
		bam_index_md5 = "work/{sample}.bam.bai.md5"
	output:
		md5_check_bam = temp("temp/{sample}.bam.md5.OK"),
		md5_check_bam_index = temp("temp/{sample}.bam.bai.md5.OK")
	log:
		"logs/{sample}.md5.log"
	shell:
		"""
		cd work; if md5sum --status -c {wildcards.sample}.bam.md5 2> ../{log};  
		then echo 'bam file md5 checksum: OK' > ../{output.md5_check_bam}; 
		else echo 'bam file md5 checksum: failed' > ../{log}; 
 		fi
		if md5sum --status -c {wildcards.sample}.bam.bai.md5 2> ../{log}; 
                then echo 'bam index file md5 checksum: OK' > ../{output.md5_check_bam_index}; 
                else echo 'bam index file md5 checksum: failed' > ../{log}; 
                fi
		"""

#Perform samtools quickcheck on bam and measure size in GB
rule generate_bam_quickcheck:
	input:
		bam = "work/{sample}.bam"
	output:
		quickcheck = temp("temp/{sample}.bam.quickcheck.OK"),
		bam_size = temp("temp/{sample}.bam.size")
	log:
		"logs/{sample}.quickcheck.log"
	conda:
		"environment.yaml"
	shell:
		"""
		if samtools quickcheck {input.bam} 2> {log};   
		then echo 'bam file quickcheck: OK' > {output.quickcheck}; 
		else echo 'bam file quickcheck: failed' > {log};  
		fi
		printf '%s ' 'Initial bam file size (Gb):' > {output.bam_size}
		printf '%0.2f\n' `du -bs {input.bam} | awk '{{$1=$1/2^30}}1' | cut -f1 -d' '` >> {output.bam_size}
		"""

#Generate flagstat for bam - to be contrasted with the equivalent produced from cram
rule generate_bam_flagstat:
	input:
		bam = "work/{sample}.bam",
	output:
		temp("temp/{sample}.bam.flagstat")
	resources:
		mem_gb = 2,runtime = 180
	log:
		"logs/{sample}.flagstat.bam.log"
	conda:
		"environment.yaml"
	shell:
		"samtools flagstat {input.bam} > {output} 2> {log}"

#Perform bam to cram conversion then remove bam and associated files to free up space	
rule convert_bam_to_cram:
	input:
		#Require the input bam, and that md5s and quickcheck have been performed
		bam = "work/{sample}.bam",
		md5 = "temp/{sample}.bam.md5.OK",
		quickcheck = "temp/{sample}.bam.quickcheck.OK"
	output:
		cram = "work/{sample}.cram",
		cram_index = "work/{sample}.cram.crai",
		cram_flagstat = "work/{sample}.cram.flagstat",
		version_info = temp("temp/{sample}.version.info")
	params:
		ref=config["reference_genome"]
	resources:
		mem_gb = 5,runtime = 1000
	log:
		"logs/{sample}.cram.log"
	conda:
		"environment.yaml"
	shell:
		"python scripts/bam_to_cram.py {wildcards.sample} {params.ref} {input.bam} {output.cram} {log} {output.cram_flagstat}"

#Perform quickcheck, compare bam and cram flagstats, and determine cram size
rule validate_cram:
	input:
		cram = "work/{sample}.cram",
		bam_flagstat = "temp/{sample}.bam.flagstat",
		cram_flagstat = "work/{sample}.cram.flagstat",
	output:
		quickcheck = temp("temp/{sample}.cram.quickcheck.OK"),
		flagstat_check = temp("temp/{sample}.flagstats.comparison.OK"),
		cram_size = temp("temp/{sample}.cram.size")
	log:
		"logs/{sample}.final.validation.log"
	conda:
		"environment.yaml"
	shell:
		"""
		if samtools quickcheck {input.cram} 2> {log};  
		then echo 'cram file quickcheck: OK' > {output.quickcheck}; 
		else echo 'cram file quickcheck: failed' > {log};  
		fi
		if cmp {input.bam_flagstat} {input.cram_flagstat} 2> {log}; 
		then echo 'flagstat comparison: OK' > {output.flagstat_check}; 
		else echo 'flagstat comparison: failed' > {log}; 
		fi
		printf '%s ' 'Output cram file size (Gb):' > {output.cram_size}
                printf '%0.2f\n' `du -bs {input.cram} | awk '{{$1=$1/2^30}}1' | cut -f1 -d' '` >> {output.cram_size}
		"""

#Produce md5s from cram files
rule generate_cram_md5:
	input:
		cram = "work/{sample}.cram",
		cram_index = "work/{sample}.cram.crai"
	output:
		cram_md5 = "work/{sample}.cram.md5",
		cram_index_md5 = "work/{sample}.cram.crai.md5"
	log:
		"logs/{sample}.cram.md5.log"
	run:
		shell("cd work; md5sum {wildcards.sample}.cram > {wildcards.sample}.cram.md5 2> ../{log}")
		shell("cd work; md5sum {wildcards.sample}.cram.crai > {wildcards.sample}.cram.crai.md5 2> ../{log}")

#Transfer cram and associated md5 to destination if all previous checks successful, then remove from work directory 
rule deposit_cram:
	input:
		bam_md5_check = "temp/{sample}.bam.md5.OK",
		bam_index_md5_check = "temp/{sample}.bam.bai.md5.OK",
		quickcheck = "temp/{sample}.cram.quickcheck.OK",
		cram = "work/{sample}.cram",
		cram_index = "work/{sample}.cram.crai",
		cram_md5 = "work/{sample}.cram.md5",
		cram_index_md5 = "work/{sample}.cram.crai.md5",
		cram_flagstat = "work/{sample}.cram.flagstat"
	output:
		cram = "{dir}/{sample}.cram",
		cram_index = "{dir}/{sample}.cram.crai",
		cram_md5 = "{dir}/{sample}.cram.md5",
		cram_index_md5 = "{dir}/{sample}.cram.crai.md5",
		cram_flagstat = "{dir}/{sample}.cram.flagstat"
	resources:
		runtime = 300
	run:
		shell("rsync -v {input.cram} {output.cram}")
		shell("rsync -v {input.cram_index} {output.cram_index}")
		shell("rsync -v {input.cram_md5} {output.cram_md5}")
		shell("rsync -v {input.cram_index_md5} {output.cram_index_md5}")
		shell("rsync -v {input.cram_flagstat} {output.cram_flagstat}")
		shell("rm work/{wildcards.sample}*")

#Check md5s of newly transferred crams, and transfer log file if OK
rule check_cram_md5:
	input: 
		cram_md5 = "{dir}/{sample}.cram.md5",
		cram_index_md5 = "{dir}/{sample}.cram.crai.md5"
	output:
		md5_check_cram = "{dir}/{sample}.cram.md5.ok",
		md5_check_cram_index = "{dir}/{sample}.cram.crai.md5.ok"
	run:
		shell("cd {wildcards.dir}; if md5sum --status -c {wildcards.sample}.cram.md5;then echo 'cram file md5 checksum: OK' > {wildcards.sample}.cram.md5.ok;else echo 'cram file md5 checksum: failed';fi")
		shell("cd {wildcards.dir}; if md5sum --status -c {wildcards.sample}.cram.crai.md5;then echo 'cram file index md5 checksum: OK' > {wildcards.sample}.cram.crai.md5.ok;else echo 'cram file md5 checksum: failed';fi")

#Place conversion log in target directory if cram md5 checks were successful
rule output_conversion_log:
	input:
		md5_check_cram = "{dir}/{sample}.cram.md5.ok",
		md5_check_cram_index = "{dir}/{sample}.cram.crai.md5.ok",
		version_info = "temp/{sample}.version.info",
		bam_size = "temp/{sample}.bam.size",
                cram_size = "temp/{sample}.cram.size"
	output:
		conv_log = "{dir}/{sample}.conv.log"
	run:
		#Produce conversion log
		shell("printf '%s ' 'Conversion complete at:' > temp/{wildcards.sample}.timestamp")
 		shell("echo `date` >> temp/{wildcards.sample}.timestamp")
           	shell("cat temp/{wildcards.sample}.timestamp {input.version_info} {input.bam_size} {input.cram_size} > {output.conv_log}")
               	shell("rm temp/{wildcards.sample}.timestamp")
			
