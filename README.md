A Snakemake pipeline for automated conversion of bam files to cram format.

* To run, first initialise a conda environment with Snakemake available. 

* Generate a config file by running 'python generate_config.py bam_paths.txt' where 'bam_paths.txt' contains absolute paths to the bam files to be converted (one per line)  
* Launch the pipeline using 'snakemake --profile cluster-qsub --cluster-config cluster_config.yaml --use-conda'

Once verified, newly converted cram files will be placed in the same directory as the original bam files. 

Note that the pipeline expects each bam file to be accompanied by an index 'sample.bam.bai', and both the bam and index to have corresponding md5 checksums with suffix '.md5'. 

The original bam files will not be deleted by the pipeline, so ensure that sufficient space is available to hold both the bam and cram files (cram files are typically around 65% the size of the initial bam files).

A conversion log 'sample.conv.log' will be written only when the process is complete and the cram file md5sums checked. Ensuring each cram file has an accompanying log therefore provides a helpful sanity check that the pipeline has completed successfully. 

Dependent on the size and number of input files, the pipeline may take some time to complete, and so it is advisable to launch within a screen session.

Human hg38 is set as the default build, but an alternative can be specified. See 'python generate_config.py --help'. 

(Pipeline based on earlier scripts by Elvina Gountouna and Kevin Donnelly)
