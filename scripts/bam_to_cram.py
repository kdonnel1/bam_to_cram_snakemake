#!/bin/python

import sys
import os
import subprocess

sample = (sys.argv[1])
ref = (sys.argv[2])
bam = (sys.argv[3])
cram = (sys.argv[4])
log = (sys.argv[5])
flagstat = (sys.argv[6])

subprocess.call("printf '%s ' 'Using' >> temp/" + sample + ".version.info", shell = True)

subprocess.call("samtools --version | head -n1 >>  temp/" + sample + ".version.info", shell = True)

subprocess.call("printf '%s ' 'Reference:' >> temp/" + sample + ".version.info", shell = True)

subprocess.call("echo " + ref + " >> temp/" + sample + ".version.info", shell = True)

subprocess.call("samtools view -@ $NSLOTS -h -C " + bam +  " -o " + cram + " -T " + ref + " 2> " + log, shell = True)

subprocess.call("samtools index " + cram + " 2> " + log, shell = True)

subprocess.call("samtools flagstat " + cram + " > " + flagstat + " 2> " + log, shell = True)

